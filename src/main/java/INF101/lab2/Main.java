package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;

    public static void main(String[] args) {
        Main.pokemon1 = new Pokemon("Pikachu");
        Main.pokemon2 = new Pokemon("Oddish");

        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println();

        while (true) {
            pokemon1.attack(pokemon2);
            if (!pokemon2.isAlive()) {
                break;
            }

            pokemon2.attack(pokemon1);
            if (!pokemon1.isAlive()) {
                break;
            }
        }
    }
}
